# Debugging CSS

In the final article of this module, we take a look at the principles of debugging CSS, including exploring CSS applied to a page, and other tools which can help you find errors in CSS code.

## Table of Contents

- [Debugging CSS](#debugging-css)
  - [Table of Contents](#table-of-contents)
  - [CSS and debugging](#css-and-debugging)
    - [Inspecting the DOM and CSS](#inspecting-the-dom-and-css)
    - [CSS validation](#css-validation)
  - [Summary](#summary)
  - [References](#references)

## CSS and debugging

Just like HTML, CSS is *permissive*. In CSS's case, if a declaration is invalid (contains a syntax error, or the browser doesn't support that feature), the browser just ignores it completely and moves on to the next one it finds.

If a selector is invalid, then it doesn't select anything, and the whole rule does nothing — again, the browser just moves on to the next rule.

This is great in a lot of ways — in most cases your content will be shown to your users, even if it isn't styled quite right. But this isn't very helpful when you are trying to debug the problem and you don't even get any kind of error message to help you find it. This is even more of a pain when the content isn't viewable by your users — perhaps a critical style isn't being applied, resulting in the layout going badly wrong?

Fortunately there are some tools available to help you. Let's look at these now.

### Inspecting the DOM and CSS

Nowadays, all web browsers provide developer tools made to help you inspect and understand web pages. Among the various tools they provide, there are two that are available in all browsers: The DOM Inspector and the CSS Editor, which are available in Firefox in the page inspector tool. We already looked at the DOM Inspector in Debugging HTML and how it can be used to inspect HTML. Here we'll look at this *and* the CSS Editor, and how to use them together to debug CSS problems.

> **Note**: In the following example, we are using Firefox, but all browsers provide the same kind of tools — they might just be available in slightly different places.

When going through this example. We'd advise you to make a copy of the HTML and CSS files below, and implement the fixes locally.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Debugging CSS</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <h1>My imperfect page</h1>
    </header>

    <main>

      <h2>My article</h2>

      <img src="https://mdn.mozillademos.org/files/11947/ff-logo.png" alt="firefox logo">

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla.</p>

      <ul>
        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
        <li>Integer nec odio. Praesent libero.</li>
        <li>Sed cursus ante dapibus diam.</li>
      </ul>

      <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim lacinia nunc. Curabitur tortor. Pellentesque nibh. Aenean quam. In scelerisque sem at dolor.</p>

    </main>

    <footer>
      <p>&copy;1978 Chris' brain</p>
    </footer>  
  </body>
</html>
```

```css
/* General styles */

html {
  font-family: 'Helvetica neue', Arial, 'sans serif';
  font-size: 10px;
}

body {
  width: 80em;
  margin: 0 auto;
}

/* Typography */

h1 {
  font-size: 4em;
}

h2 {
  font-size: 3.2em;
}

pli {
  font-size: 1.7em;
}

/*+++ more specific section styles +++*/

/* header and footer */

header, footer {
  background-colour: teal;
  height: 10em;
  padding: 2em;
}

h1, footer p {
  margin: -70px;
}

h1 {
  text-align: center;
  padding: 0.5em 0;
}

/* main content */

main {
  padding: 2em;
}

main p, main li {
  line-height: 2;
}

img {
  float: right;
  margin: 1.2em 2em;
}
```

It is meant to be a simple, clear one column web page containing a simple article:

![Page fixed](img/page-fixed.png)

At the moment however it is a bit of a mess:

![Page broken](img/page-broken.png)

Let's start investigating the page with the page inspector's features. In Firefox you can open the page inspector using Cmd/Ctrl + Shift + I (or by choosing *Tools* > *Web Developer* > *Inspector* from the menu), which will give you a set of tools open in a window on the bottom of the browser like so:

![Page inspector](img/page-inspector.png)

If you click on an element inside the DOM Inspector on the left, the CSS editor on the right will update to show all the CSS rules applied to that element. This is really useful, especially as any invalid properties appear with a line through them and a little warning symbol next to them. This will come in handy below!

![Show invalid property](img/show-invalid-property.png)

> **Note**: Each property also has a checkbox next to it, which can be checked/unchecked to allow you to enable/disable your CSS property by property. This is also very useful for figuring out what might be causing an error.

So, with the tool basics outlined, let's try to find our errors. In each case, you should try clicking on the element where the fault is, to see what the applied CSS looks like.

1. The `<header>` and `<footer>` elements are supposed have a background color, but no color appears.
2. The `<h1>` in the header and the `<p>` in the footer are both too high up on the page — this is especially apparent on the `<h1>`, which is nearly off the screen! You could try clicking on the `<h1>` in the developer tools inspector and then unchecking the checkboxes next to the CSS declarations shown in the rules panel to find out which one is causing the problem.
3. The `<img>` is supposed to be floated down and to the right so it sits to the right of some of the text, but it isn't — it's directly above all of the text.
4. The text in the `<main>` content area is far too small — the paragraphs and list items should have a larger font-size applied to it, but it doesn't seem to have been applied to either. Hint: This one is a bit harder, as it is a problem with the selector rather than a property.  You may have to study the source code to find this — you can find it in the Style Editor in Firefox's developer tools.

### CSS validation

We already looked at the W3C HTML Validator, but they have a CSS Validator available too. This works in the same way, allowing you to validate CSS at a particular URL, by uploading a local file, or by direct CSS input.

![CSS validator](img/css-validator.png)

## Summary

Well done for completing the last article of the first CSS module! Now that you've come this far, you can then start exploring some other CSS and HTML modules.

## References

1. https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Debugging_CSS
