# CSS Selectors

In CSS, selectors are used to target the HTML elements on our web pages that we want to style. There are a wide variety of CSS selectors available, allowing for fine grained precision when selecting elements to style. In the next few articles we'll run through the different types in great detail, seeing how they work.

## Table of Contents

- [CSS Selectors](#css-selectors)
  - [Table of Contents](#table-of-contents)
  - [The basics](#the-basics)
    - [Different types of selectors](#different-types-of-selectors)
  - [Simple selectors](#simple-selectors)
    - [Type selectors aka element selectors](#type-selectors-aka-element-selectors)
    - [Class selectors](#class-selectors)
    - [ID selectors](#id-selectors)
    - [Universal selector](#universal-selector)
  - [Attribute selectors](#attribute-selectors)
    - [Presence and value attribute selectors](#presence-and-value-attribute-selectors)
    - [Substring value attribute selectors](#substring-value-attribute-selectors)
  - [Pseudo-classes and pseudo-elements](#pseudo-classes-and-pseudo-elements)
    - [Pseudo-classes](#pseudo-classes)
    - [Pseudo-elements](#pseudo-elements)
  - [Combinators and selector lists](#combinators-and-selector-lists)
  - [Groups of selectors on one rule](#groups-of-selectors-on-one-rule)
  - [What's next](#whats-next)
  - [References](#references)

## The basics

In the last article we explained general CSS syntax and terminology in detail. To recap, selectors are one part of a CSS rule and come just before CSS declaration blocks.

![CSS Ruleset](img/css-ruleset.png)

### Different types of selectors

Selectors can be divided into the following categories:

- **Simple selectors**: Match one or more elements based on element type, `class`, or `id`.
- **Attribute selectors**: Match one or more elements based on their attributes/attribute values.
- **Pseudo-classes**: Match one or more elements that exist in a certain state, such as an element that is being hovered over by the mouse pointer, or a checkbox that is currently disabled or checked, or an element that is the first child of its parent in the DOM tree.
- **Pseudo-elements**: Match one or more parts of content that are in a certain position in relation to an element, for example the first word of each paragraph, or generated content appearing just before an element.
- **Combinators**: These are not exactly selectors themselves, but ways of combining two or more selectors in useful ways for very specific selections. So for example, you could select only paragraphs that are direct descendants of divs, or paragraphs that come directly after headings.
- **Multiple selectors**: Again, these are not separate selectors; the idea is that you can put multiple selectors on the same CSS rule, separated by commas, to apply a single set of declarations to all the elements selected by those selectors.

## Simple selectors

### Type selectors aka element selectors

This selector is just a case-insensitive match between the selector name and a given HTML element name. This is the simplest way to target all elements of a given type. Let's take a look at an example:

Here is some HTML:

```html
<p>What color do you like?</p>
<div>I like blue.</div>
<p>I prefer BLACK!</p>
```

A simple style sheet:

```css
/* All p elements are red */
p {
  color: red;
}

/* All div elements are blue */
div {
  color: blue;
}
```

### Class selectors

The class selector consists of a dot, '`.`', followed by a class name. A class name is any value, without spaces, placed within an HTML `class` attribute. It is up to you to choose a name for the class. It is also noteworthy that multiple elements in a document can have the same class value, and a single element can have multiple class names separated by white space. Here's a quick example:

Here is some HTML:

```html
<ul>
  <li class="first done">Create an HTML document</li>
  <li class="second done">Create a CSS style sheet</li>
  <li class="third">Link them all together</li>
</ul>
```

A simple style sheet that styles two of these classes:

```css
/* The element with the class "first" is bolded */
.first {
  font-weight: bold;
}

/* All the elements with the class "done" are strikethrough */
.done {
  text-decoration: line-through;
}
```

### ID selectors

The ID selector consists of a hash/pound symbol (`#`), followed by the ID name of a given element. Any element can have a unique ID name set with the `id` attribute. It is up to you to choose an ID name. It's the most efficient way to select a single element.

> **Important**: An ID name must be unique in the document. Behaviors regarding duplicated IDs are unpredictable. For example, in some browsers, only the first instance is counted, and the rest are ignored.

Let's look at a quick example — here is some HTML:

```html
<p id="polite"> — "Good morning."</p>
<p id="rude"> — "Go away!"</p>
```

A simple style sheet:

```css
#polite {
  font-family: cursive;
}

#rude {
  font-family: monospace;
  text-transform: uppercase;
}
```

### Universal selector

The universal selector (*) is the ultimate joker. It allows selecting all elements on a page. As it is rarely used to apply a style to every element on a page, it is often used in combination with other selectors.

> **Important**: Take care when using the universal selector. As it applies to all elements, using it in large web pages can have a perceptible impact on performance: web pages display slower than expected. There are not many instances where you'd use this selector.

Now for an example; first some HTML:

```html
<div>
  <p>I think the containing box just needed
  a <strong>border</strong> or <em>something</em>,
  but this is getting <strong>out of hand</strong>!</p>
</div>
```

And a simple style sheet:

```css
* {
  padding: 5px;
  border: 1px solid black;
  background: rgba(255,0,0,0.25)
}
```

## Attribute selectors

Attribute selectors are a special kind of selector that will match elements based on their attributes and attribute values. Their generic syntax consists of square brackets (`[]`) containing an attribute name followed by an optional condition to match against the value of the attribute. Attribute selectors can be divided into two categories depending on the way they match attribute values: **Presence and value** attribute selectors and **Substring value** attribute selectors.

### Presence and value attribute selectors

These attribute selectors try to match an exact attribute value:

- `[attr]` : This selector will select all elements with the attribute `attr`, whatever its value.
- `[attr=val]` : This selector will select all elements with the attribute `attr`, but only if its value is val.
- `[attr~=val]`: This selector will select all elements with the attribute `attr`, but only if val is one of a space-separated list of words contained in `attr`'s value.

Let's look at an example featuring the following HTML snippet:

```html
Ingredients for my recipe: <i lang="fr-FR">Poulet basquaise</i>
<ul>
  <li data-quantity="1kg" data-vegetable>Tomatoes</li>
  <li data-quantity="3" data-vegetable>Onions</li>
  <li data-quantity="3" data-vegetable>Garlic</li>
  <li data-quantity="700g" data-vegetable="not spicy like chili">Red pepper</li>
  <li data-quantity="2kg" data-meat>Chicken</li>
  <li data-quantity="optional 150g" data-meat>Bacon bits</li>
  <li data-quantity="optional 10ml" data-vegetable="liquid">Olive oil</li>
  <li data-quantity="25cl" data-vegetable="liquid">White wine</li>
</ul>
```

And a simple style sheet:

```css
/* All elements with the attribute "data-vegetable"
   are given green text */
[data-vegetable] {
  color: green;
}

/* All elements with the attribute "data-vegetable"
   with the exact value "liquid" are given a golden
   background color */
[data-vegetable="liquid"] {
  background-color: goldenrod;
}

/* All elements with the attribute "data-vegetable",
   containing the value "spicy", even among others,
   are given a red text color */
[data-vegetable~="spicy"] {
  color: red;
}
```

> **Note**: The `data-*` attributes seen in this example are called data attributes. They provide a way to store custom data in an HTML attribute so it can then be easily extracted and used. See How to use data attributes for more information.

### Substring value attribute selectors

Attribute selectors in this class are also known as "RegExp-like selectors", because they offer flexible matching in a similar fashion to regular expression (but to be clear, these selectors are not true regular expression):

- `[attr^=val]` : This selector will select all elements with the attribute `attr` for which the value starts with `val`.
- `[attr$=val]` : This selector will select all elements with the attribute `attr` for which the value ends with `val`.
- `[attr*=val]` : This selector will select all elements with the attribute `attr` for which the value contains the substring `val`. (A substring is simply part of a string, e.g. "cat" is a substring in the string "caterpillar".)

> **Note**: There's also a very specific alternative version of the first option — `[attr|=val]`.  This selector selects all elements with the attribute `attr` for which the value is exactly `val` or starts with `val-` (the hyphen here isn't a mistake). This was implemented specifically to match language codes, e.g. lang="en" or lang="en-US", and you probably won't use it very often.

Let's continue our previous example and add the following CSS rules:

```css
/* Classic usage for language selection */
[lang|="fr"] {
  font-weight: bold;
}

/* All elements with the attribute "data-quantity", for which
   the value starts with "optional" */
[data-quantity^="optional"] {
  opacity: 0.5;
}

/* All elements with the attribute "data-quantity", for which
   the value ends with "kg" */
[data-quantity$="kg"] {
  font-weight: bold;
}

/* All elements with the attribute "data-vegetable" containing
   the substring "not spicy" are turned back to green */
[data-vegetable*="not spicy"] {
  color: green;
}
```

## Pseudo-classes and pseudo-elements

These don't select elements, but rather certain parts of elements, or elements only in certain contexts. They come in two main types: **pseudo-classes** and **pseudo-elements**.

### Pseudo-classes

A CSS pseudo-class is a keyword added to the end of a selector, preceded by a colon (`:`), which is used to specify that you want to style the selected element but only when it is in a *certain state*. For example, you might want to style a link element only when it is being hovered over by the mouse pointer, or a checkbox when it is disabled or checked, or an element that is the first child of its parent in the DOM tree.

We will not dig into every pseudo-class right now, it is not our objective to teach you everything in *exhaustive detail*. You'll certainly come across some of these in more detail later, where appropriate.

For now, let's just see a simple example of how to use these. First, an HTML snippet:

```html
<a href="https://developer.mozilla.org/" target="_blank">Mozilla Developer Network</a>
```

Then, some CSS rules:

```css
/* These styles will style our link
   in all states */
a {
  color: blue;
  font-weight: bold;
}

/* We want visited links to be the same color
   as non visited links */
a:visited {
  color: blue;
}

/* We highlight the link when it is
   hovered over (mouse over), activated (mouse down)
   or focused (keyboard) */
a:hover,
a:active,
a:focus {
  color: darkred;
  text-decoration: none;
}
```

### Pseudo-elements

Pseudo-elements are very much like pseudo-classes, but they have differences. They are keywords, this time preceded by two colons (`::`), that can be added to the end of selectors to select a certain part of an element.

- `::after`
- `::before`
- `::first-letter`
- `::first-line`
- `::selection`
- `::backdrop`

They all have some very specific behaviors and interesting features, but digging into them all in detail is beyond our scope for now.

Here we'll just show a simple CSS example that selects the space just after all absolute links and adds an arrow in that space:

```html
<ul>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/CSS">CSS</a> defined in the MDN glossary.</li>
  <li><a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML">HTML</a> defined in the MDN glossary.</li>
</ul>
```

Let's add this CSS rule:

```css
/* All elements with an attribute "href" with values
   starting with "http" will have an arrow added after their
   content (to indicate they are external links) */
[href^=http]::after {
  content: '⤴';
}
```

## Combinators and selector lists

Using one selector at a time is useful, but can be inefficient in some situations. CSS selectors become even more useful when you start combining them to perform fine-grained selections. CSS has several ways to select elements based on how they are related to one another. Those relationships are expressed with *combinators* as follows (A and B represent any selector seen above):

| Name | Syntax | Selects |
| :--- | :----: | :------ |
| Selector list | `A, B` | Any element matching A and/or B (see Groups of selectors on one rule, below - **Group of Selectors** is not considered to be a combinator). |
| Descendant combinator | `A B` | Any element matching B that is a *descendant* of an element matching A (that is, a child, or a child of a child, etc.). the combinator is one or more spaces or dual greater than signs. |
| Child combinator | `A > B` | Any element matching B that is a *direct child* of an element matching A. |
| Adjacent sibling combinator | `A + B` | Any element matching B that is the next *sibling* of an element matching A (that is, the next child of the same parent). |
General sibling combinator | `A ~ B` | Any element matching B that is one of the next *siblings* of an element matching A (that is, one of the next children of the same parent). |

Let's look at an example with all of this working together:

```html
<table lang="en-US" class="with-currency">
  <thead>
    <tr>
      <th scope="col">Product</th>
      <th scope="col">Qty</th>
      <th scope="col">Price</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <th colspan="2" scope="row">Total:</th>
      <td>148.55</td>
    </tr>
  </tfoot>
  <tbody>
    <tr>
      <td>Lawnchair</td>
      <td>1</td>
      <td>137.00</td>
    </tr>
    <tr>
      <td>Marshmallow rice bar</td>
      <td>2</td>
      <td>1.10</td>
    </tr>
    <tr>
      <td>Book</td>
      <td>1</td>
      <td>10.45</td>
    </tr>
  </tbody>
</table>
```

Then, let's use the following style sheet:

```css
/* Basic table setup */
table {
  font: 1em sans-serif;
  border-collapse: collapse;
  border-spacing: 0;
}

/* All <td>s within a <table> and all <th>s within a <table>
   Comma is not a combinator, it just allows you to target
   several selectors with the same CSS ruleset */
table td, table th {
  border: 1px solid black;
  padding: 0.5em 0.5em 0.4em;
}

/* All <th>s within <thead>s that are within <table>s */
table thead th {
  color: white;
  background: black;
}

/* All <td>s preceded by another <td>,
   within a <tbody>, within a <table> */
table tbody td + td {
  text-align: center;
}

/* All <td>s that are a last child,
   within a <tbody>, within a <table> */
table tbody td:last-child {
  text-align: right;
}

/* All <th>s, within a <tfoot>s, within a <table> */
table tfoot th {
  text-align: right;
  border-top-width: 5px;
  border-left: none;
  border-bottom: none;
}

/* All <td>s preceded by a <th>, within a <table> */
table th + td {
  text-align: right;
  border-top-width: 5px;
  color: white;
  background: black;
}

/* All pseudo-elements "before" <td>s that are a last child,
   appearing within elements with a class of "with-currency" that
   also have an attribute "lang" with the value "en-US" */
.with-currency[lang="en-US"] td:last-child::before {
  content: '$';
}

/* All pseudo-elements "after" <td>s that are a last child,
   appearing within elements with the class "with-currency" that
   also have an attribute "lang" with the value "fr" */
.with-currency[lang="fr"] td:last-child::after {
  content: ' €';
}
```

## Groups of selectors on one rule

You have seen multiple examples of this in action already, but let's spell it out for clarification. You can write groups of selectors separated by commas to apply the same rule to multiple sets of selected elements at once. For example:

```css
p, li {
  font-size: 1.6em;
}
```

Or this:

```css
h1, h2, h3, h4, h5, h6 {
  font-family: helvetica, 'sans serif';
}
```

> **Important**: If a single selector is not supported by the browser, then the whole selector list gets ignored.
>
> The :is()  pseudo-class is not subject to this limitation, but isn't yet widely supported at the time of writing.

## What's next

Congratulations — you've come to the end of our rather long journey into learning about CSS selectors. Even the most skilled web developers are still amazed by what's possible using selectors. However, don't feel bad if you can't remember all the options.

In our next article, we'll turn to another really important fundamental CSS topic — the kind of values properties can have, and what units are involved in expressing the length, color, or any other values you want.  Let's explore CSS values and units.

## References

1. https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Selectors
