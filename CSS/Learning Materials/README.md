# Introduction to CSS

CSS (Cascading Style Sheets) is used to style and layout web pages — for example, to alter the font, colour, size and spacing of your content, split it into multiple columns, or add animations and other decorative features. This module gets you started on the path to CSS mastery with the basics of how it works, including selectors and properties, writing CSS rules, applying CSS to HTML, how to specify length, colour, and other units in CSS, cascade and inheritance, and debugging CSS.

## Prerequisites

Before starting this module, you should have:

1. Basic familiarity with using computers, and using the Web passively (i.e. just looking at it, consuming the content.)
2. A basic work environment set up as detailed in Installing basic software, and an understanding of how to create and manage files, as detailed in Dealing with files.
3. Basic familiarity with HTML, as discussed in the Introduction to HTML module.

## Guides

This module contains the following articles, which will take you through all the basic theory of CSS, and provide ample opportunity for you to test out some skills.

### [How CSS works](01_How_CSS_works.md)

In this module we start off with a theoretical grounding, looking at what CSS is, how the browser turns HTML into a DOM, how CSS is applied to parts of the DOM, some very basic syntax examples, and what code is used to actually include our CSS in our web page.

### [CSS syntax](02_CSS_syntax.md)

Next up, we dive into CSS syntax in a lot more detail, looking at how properties and their values form into declarations, multiple declarations form into declaration blocks, and declaration blocks and selectors form into complete CSS rules. We round off the article by looking at other CSS syntax features such as comments and whitespace.

### [Selectors](03_Selectors.md)

Selectors are mentioned in the previous article, but in this guide we go into a lot more detail, showing what selector types are available and how they work.

### [CSS values and units](04_CSS_values_and_units.md)

There are many types of CSS property values to consider, from numerical values to colors to functions that perform a certain action (like embedding a background image or rotating an element.) Some of these rely on particular units for specifying the exact values they are representing — do you want your box to be 30 pixels wide, or 30 centimetres, or 30 ems? In this guide, we look at more common values like length, colour and simple functions, as well as exploring less common units like degrees, and even unitless numerical values.

### [Cascade and inheritance](05_Cascade_and_inheritance.md)

CSS has two different but related systems to resolve situations where you have selector conflicts (different selectors select the same elements; which one wins and ends up being applied?) and elements nested inside other elements (some of the styling applied to the parent elements makes sense to be inherited by the child elements; some doesn't.) This article covers both systems in enough detail to be useful but not overwhelming.

### [The box model](06_The_box_model.md)

The CSS box model is the foundation of layout on the Web — each element is represented as a rectangular box, with the box's content, padding, border, and margin built up around one another like the layers of an onion. As a browser renders a web page, it works out what styles are applied to the content of each box, how big the surrounding onion layers are, and where the boxes sit in relation to one another. Before understanding how to create CSS layouts, you need to understand the box model.

### [Debugging CSS](07_Debugging_CSS.md)

In the final article of this module, we take a look at the basics of debugging CSS, including exploring the CSS applied to a page, and other tools that can help you find errors in your CSS code.

## Table of Contents

- [How CSS works](01_How_CSS_works.md#how-css-works)
  - [Table of Contents](01_How_CSS_works.md#table-of-contents)
  - [What is CSS?](01_How_CSS_works.md#what-is-css)
  - [How does CSS affect HTML?](01_How_CSS_works.md#how-does-css-affect-html)
    - [A quick CSS example](01_How_CSS_works.md#a-quick-css-example)
  - [How does CSS actually work?](01_How_CSS_works.md#how-does-css-actually-work)
  - [About the DOM](01_How_CSS_works.md#about-the-dom)
    - [DOM representation](01_How_CSS_works.md#dom-representation)
    - [Applying CSS to the DOM](01_How_CSS_works.md#applying-css-to-the-dom)
  - [How to apply your CSS to your HTML](01_How_CSS_works.md#how-to-apply-your-css-to-your-html)
    - [External stylesheet](01_How_CSS_works.md#external-stylesheet)
    - [Internal stylesheet](01_How_CSS_works.md#internal-stylesheet)
    - [Inline styles](01_How_CSS_works.md#inline-styles)
  - [What's next](01_How_CSS_works.md#whats-next)
  - [References](01_How_CSS_works.md#references)
- [CSS syntax](02_CSS_syntax.md#css-syntax)
  - [Table of Contents](02_CSS_syntax.md#table-of-contents)
  - [A touch of vocabulary](02_CSS_syntax.md#a-touch-of-vocabulary)
    - [CSS declarations](02_CSS_syntax.md#css-declarations)
    - [CSS declaration blocks](02_CSS_syntax.md#css-declaration-blocks)
    - [CSS selectors and rules](02_CSS_syntax.md#css-selectors-and-rules)
    - [CSS statements](02_CSS_syntax.md#css-statements)
  - [Beyond syntax: make CSS readable](02_CSS_syntax.md#beyond-syntax-make-css-readable)
    - [White space](02_CSS_syntax.md#white-space)
    - [Comments](02_CSS_syntax.md#comments)
    - [Shorthand](02_CSS_syntax.md#shorthand)
  - [What's next](02_CSS_syntax.md#whats-next)
  - [Reference](02_CSS_syntax.md#reference)
- [CSS Selectors](03_Selectors.md#css-selectors)
  - [Table of Contents](03_Selectors.md#table-of-contents)
  - [The basics](03_Selectors.md#the-basics)
    - [Different types of selectors](03_Selectors.md#different-types-of-selectors)
  - [Simple selectors](03_Selectors.md#simple-selectors)
    - [Type selectors aka element selectors](03_Selectors.md#type-selectors-aka-element-selectors)
    - [Class selectors](03_Selectors.md#class-selectors)
    - [ID selectors](03_Selectors.md#id-selectors)
    - [Universal selector](03_Selectors.md#universal-selector)
  - [Attribute selectors](03_Selectors.md#attribute-selectors)
    - [Presence and value attribute selectors](03_Selectors.md#presence-and-value-attribute-selectors)
    - [Substring value attribute selectors](03_Selectors.md#substring-value-attribute-selectors)
  - [Pseudo-classes and pseudo-elements](03_Selectors.md#pseudo-classes-and-pseudo-elements)
    - [Pseudo-classes](03_Selectors.md#pseudo-classes)
    - [Pseudo-elements](03_Selectors.md#pseudo-elements)
  - [Combinators and selector lists](03_Selectors.md#combinators-and-selector-lists)
  - [Groups of selectors on one rule](03_Selectors.md#groups-of-selectors-on-one-rule)
  - [What's next](03_Selectors.md#whats-next)
  - [References](03_Selectors.md#references)
- [CSS values and units](04_CSS_values_and_units.md#css-values-and-units)
  - [Table of Contents](04_CSS_values_and_units.md#table-of-contents)
  - [Numeric values](04_CSS_values_and_units.md#numeric-values)
    - [Length and size](04_CSS_values_and_units.md#length-and-size)
    - [Unitless values](04_CSS_values_and_units.md#unitless-values)
      - [Unitless line height](04_CSS_values_and_units.md#unitless-line-height)
      - [Number of animations](04_CSS_values_and_units.md#number-of-animations)
  - [Percentages](04_CSS_values_and_units.md#percentages)
  - [Colors](04_CSS_values_and_units.md#colors)
    - [Keywords](04_CSS_values_and_units.md#keywords)
    - [Hexadecimal values](04_CSS_values_and_units.md#hexadecimal-values)
    - [RGB](04_CSS_values_and_units.md#rgb)
    - [HSL](04_CSS_values_and_units.md#hsl)
    - [RGBA and HSLA](04_CSS_values_and_units.md#rgba-and-hsla)
    - [Opacity](04_CSS_values_and_units.md#opacity)
  - [Functions](04_CSS_values_and_units.md#functions)
  - [Summary](04_CSS_values_and_units.md#summary)
  - [References](04_CSS_values_and_units.md#references)
- [Cascade and inheritance](05_Cascade_and_inheritance.md#cascade-and-inheritance)
  - [Table of Contents](05_Cascade_and_inheritance.md#table-of-contents)
  - [The cascade](05_Cascade_and_inheritance.md#the-cascade)
    - [Importance](05_Cascade_and_inheritance.md#importance)
    - [Specificity](05_Cascade_and_inheritance.md#specificity)
    - [Source order](05_Cascade_and_inheritance.md#source-order)
    - [A note on rule mixing](05_Cascade_and_inheritance.md#a-note-on-rule-mixing)
  - [Inheritance](05_Cascade_and_inheritance.md#inheritance)
    - [Controlling inheritance](05_Cascade_and_inheritance.md#controlling-inheritance)
      - [inherit](05_Cascade_and_inheritance.md#inherit)
      - [initial](05_Cascade_and_inheritance.md#initial)
      - [unset](05_Cascade_and_inheritance.md#unset)
      - [revert](05_Cascade_and_inheritance.md#revert)
    - [Resetting all property values](05_Cascade_and_inheritance.md#resetting-all-property-values)
  - [What's next](05_Cascade_and_inheritance.md#whats-next)
  - [References](05_Cascade_and_inheritance.md#references)
- [The box model](06_The_box_model.md#the-box-model)
  - [Table of Contents](06_The_box_model.md#table-of-contents)
  - [Box properties](06_The_box_model.md#box-properties)
  - [Advanced box manipulation](06_The_box_model.md#advanced-box-manipulation)
    - [Overflow](06_The_box_model.md#overflow)
    - [Background clip](06_The_box_model.md#background-clip)
    - [Outline](06_The_box_model.md#outline)
  - [Types of CSS boxes](06_The_box_model.md#types-of-css-boxes)
  - [What's next](06_The_box_model.md#whats-next)
  - [References](06_The_box_model.md#references)
- [Debugging CSS](07_Debugging_CSS.md#debugging-css)
  - [Table of Contents](07_Debugging_CSS.md#table-of-contents)
  - [CSS and debugging](07_Debugging_CSS.md#css-and-debugging)
    - [Inspecting the DOM and CSS](07_Debugging_CSS.md#inspecting-the-dom-and-css)
    - [CSS validation](07_Debugging_CSS.md#css-validation)
  - [Summary](07_Debugging_CSS.md#summary)
  - [References](07_Debugging_CSS.md#references)

## References

1. https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS
