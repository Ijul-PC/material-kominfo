# Code Questions

1. What is the value of `foo`?

    ```js
    var foo = 10 + '20';
    ```

   - **Answer**: `'1020'`, because of type coercion from Number to String

2. What will be the output of the code below?

    ```js
    console.log(0.1 + 0.2 == 0.3);
    ```

   - **Answer**: `false`

3. How would you make this work?

    ```js
    add(2, 5); // 7
    add(2)(5); // 7
    ```

   - **Answer**: A general solution for any number of parameters

      ```js
      let sum = (arr) => arr.reduce((a, b) => a + b);
      let addGenerator = (numArgs, prevArgs) => {
        return function () {
          let totalArgs = prevArgs.concat(Array.from(arguments));
          if (totalArgs.length === numArgs) {
            return sum(totalArgs);
          }
          return addGenerator(numArgs, totalArgs);
        };
      };

      let add = addGenerator(2, []);

      add(2, 5); // 7
      add(2)(5); // 7
      add()(2, 5); // 7
      add()(2)(5); // 7
      add()()(2)(5); // 7
      ```

4. What value is returned from the following statement?

    ```js
    "i'm a lasagna hog".split("").reverse().join("");
    ```

   - **Answer**: It's actually a reverse method for a string - `'goh angasal a m'i'`

5. What is the value of `window.foo`?

    ```js
    ( window.foo || ( window.foo = "bar" ) );
    ```

   - **Answer**: Always `'bar'`

6. What is the outcome of the two alerts below?

    ```js
    var foo = "Hello";
    (function() {
      var bar = " World";
      alert(foo + bar);
    })();
    alert(foo + bar);
    ```

   - **Answer**:
     - First: `Hello World`
     - Second: Throws an exception, `ReferenceError: bar is not defined`

7. What is the value of `foo.length`?

    ```js
    var foo = [];
    foo.push(1);
    foo.push(2);
    ```

   - **Answer**: `2`, `.push` is mutable

8. What is the value of `foo.x`?

    ```js
    var foo = {n: 1};
    var bar = foo;
    foo.x = foo = {n: 2};
    ```

   - **Answer**: `undefined`. Rather, `bar.x` is `{n: 2}`. `foo.x = foo = {n: 2}` is the same as `foo.x = (foo = {n: 2})`. It is because a left term is first referenced and then a right term is evaluated when an assignment is performed in JavaScript. When `foo.x` is referenced, it refers to an original object, `{n: 1}`. So, when the result of the right term, `{n: 2}`, is evaluated, it will assigned to the original object, which is at the moment referenced by `bar`.

9. What does the following code print?

    ```js
    console.log('one');
    setTimeout(function() {
      console.log('two');
    }, 0);
    console.log('three');
    ```

   - **Answer**: `one`, `three` and `two`. It's because `console.log('two');` will be invoked in the next event loop.
