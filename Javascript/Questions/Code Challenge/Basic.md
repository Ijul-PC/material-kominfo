# JS Basic-Level Questions

1. What are the differences between `null` and `undefined`?

   - **Answer**: JavaScript has two distinct values for nothing, null and undefined.

      `undefined` means, value of the variable is not defined. JavaScript has a global variable undefined whose value is "undefined" and typeof undefined is also "undefined". Remember, undefined is not a constant or a keyword. undefined is a type with exactly one value: undefined. Assigning a new value to it does not change the value of the type undefined.

      `null` means empty or non-existent value which is used by programmers to indicate “no value”. null is a primitive value and you can assign null to any variable. null is not an object, it is a primitive value. For example, you cannot add properties to it. Sometimes people wrongly assume that it is an object, because typeof null returns "object".

2. What are the differences between `==` and `===`?

   - **Answer**: The simplest way of saying that, == will not check types and === will check whether both sides are of same type. So, == is tolerant. But under the hood it converts to its convenient type to have both in same type and then do the comparison.

3. How would you compare two objects in JavaScript?

   - **Answer**: Equality check will check whether two objects have same value for same property. To check that, you can get the keys for both the objects. If the number of properties doesn't match, these two objects are not equal. Secondly, you will check each property whether they have the same value. If all the properties have same value, they are equal.

4. Answer whether the expressions below is `truthy` or `falsy` and give explanations.

   - `'false'`

      **Answer**: `truthy`. Because, it's a string with length greater than `0`. Only empty string is `false`.

   - `' '`

      **Answer**: `truthy`. Because, it's not an empty string. There is a white space in it.

   - `{}`

      **Answer**: `truthy`. It's an object. An object without any property is an object can't be `falsy`.

   - `[]`

      **Answer**: This is also `truthy`. It's an array object (array is child of object) is truthy.

   - `new String('')`

      **Answer**: Though you are passing empty string to the string constructor, it is creating a String object. More precisely an instance of String object. It becomes an object. Hence, it is not `false`. so, it is `truthy`.

   - `new Boolean(false)`

      **Answer**: `truthy`. As it creates an instance of the Boolean object which is an object. `Object` is `truthy`.

   - `Boolean(function(){})`

      **Answer**: `true`. If you pass a truthy value to `Boolean`, it will be `true`.

5. As `[]` is true, `[] == true` should also be true, right?

   - **Answer**: You are right about first part, `[]`, empty array is an object and object is always truthy. Hence, if you use `if([]){console.log('its true')}` you will see the log.

      However, special case about `==` (double equal) is that it will do some implicit coercion.

      Since left and right side of the equality are two different types, JavaScript can't compare them directly . Hence, under the hood, JavaScript will convert them to compare. first right side of the equality will be cooereced to a number and number of `true` would be 1.

      After that, JavaScript implementation will try to convert `[]` by using `toPrimitive` (of JavaScript implementation). since `[].valueOf` is not primitive will use `toString` and will get `""`.

      Now you are comparing `"" == 1` and still left and right is not same type. Hence left side will be converted again to a number and empty string will be 0.

      Finally, they are of same type, you are comparing `0 === 1` which will be false.

6. How could you write a method on instance of a date which will give you next day?

   - **Answer**: I have to declare a method on the prototype of Date object. To get access to the current value of the instance of the date, i will use `this`.

      ```js
      Date.prototype.nextDay = function(){
        var currentDate = this.getDate();
        return new Date(this.setDate(currentDate +1));
      }

      var date = new Date();
      date; //Fri May 16 2014 20:47:14 GMT-0500 (Central Daylight Time)
      date.nextDay();//Sat May 17 2014 20:47:14 GMT-0500 (Central Daylight Time)
      ```

7. If i have a `var str = 'hello world'`, how could I get `str.reverse()` return `'dlrow olleh'`?

   - **Answer**: You have to extend the core String Object.

      ```js
      String.prototype.reverse = function(){
      return this.split('').reverse().join('');
      }

      var str = 'hello world';
      str.reverse();//"dlrow olleh"
      ```

8. How could you make this work `[1,2,3,4,5].duplicator(); // [1,2,3,4,5,1,2,3,4,5]` ?

   - **Answer**: We need to add a method in the prototype of Array object.

      ```js
      Array.prototype.duplicator = function(){
        return this.concat(this);  
      }

      [1,2,3,4,5].duplicator(); // [1,2,3,4,5,1,2,3,4,5]
      ```

9. If you want to use an arbitrary object as value of `this`, how will you do that?

   - **Answer**: There are at least three different ways to doing this by using bind, call and apply. For example, I have a method named deductMontlyFee in the object monica and by default value of this would be monica inside the method.

      ```js
      var monica = {
        name: 'Monica Geller',
        total: 400,
        deductMontlyFee: function(fee){
          this.total = this.total - fee;
          return this.name + ' remaining balance is '+ this.total;
        }
      }
      ```

      If I bind the deductMontlyFee of monica with another object `rachel` and pass rachel as first parameter of the bind function, rachel would be the value of `this`.

      ```js
      var rachel = {name: 'Rachel Green', total: 1500};
      var rachelFeeDeductor = monica.deductMonthlyFee.bind(rachel, 200);

      rachelFeeDeductor(); //"Rachel Green remaining balance is 1300"
      rachelFeeDeductor(); //"Rachel Green remaining balance is 1100"
      ```

10. Write a simple function to tell whether 2 is passed as parameter or not?

    - **Answer**: First convert arguments to an array by calling slice method on an array and pass arguments. After that simply use indexOf.

      ```js
      function isTwoPassed(){
        var args = Array.prototype.slice.call(arguments);
        return args.indexOf(2) != -1;
      }

      isTwoPassed(1,4) //false
      isTowPassed(5,3,1,2) //true
      ```

11. How could you use `Math.max` to find the max value in an array?

    - **Answer**: Use apply on Math.max and pass the array as apply takes an array of arguments. Since we are not reading anything from this or using it at all. We can simply pass null as first parameter.

      ```js
      function getMax(arr){
        return Math.max.apply(null, arr);  
      }
      ```

12. What are the outputs from expressions below? Give an explanation.

    - `typeof []`

      - **Answer**: `Object`. Actually `Array` is derived from `Object`. If you want to check array use `Array.isArray(arr)`

    - `2 + true`

      - **Answer**: `3`. The plus operator between a number and a boolean or two boolean will convert boolean to number. Hence, true converts to 1 and you get result of 2+1

    - `'6' + 9`

      - **Answer**: `69`. If one of the operands of the plus (+) operator is string it will convert other number or boolean to string and perform a concatenation. For the same reason, `"2" + true` will return "2true"

    - `4 + 3 + 2 + "1"`

      - **Answer**: `91` . The addition starts from the left, 4+3 results 7 and 7+2 is 9. So far, the plus operator is performing addition as both the operands are number. After that 9 + "1" where one of the operands is string and plus operator will perform concatenation.

    - `"1" + 2 + 4`

      - **Answer**: `"124"`. For this one `"1" + 2` will produce `"12"` and `"12" + 4` will generates `"124"`.

    - `- '34' + 10`

      - **Answer**: `-24`. minus(`-`) in front of a string is an unary operator that will convert the string to a number and will make it negative. Hence, `-'34'` becomes, `-34` and then plus (`+`) will perform simple addition as both the operands are number.

    - `+ 'dude'`

      - **Answer**: `NaN`. The plus (`+`) operator in front of a string is an unary operator that will try to convert the string to number. Here, JavaScript will fail to convert the `"dude"` to a number and will produce `NaN`.

    - `!'bang'`

      - **Answer**: `false`. `!` is `NOT`. If you put `!` in front of truthy values, it will return `false`. Using `!!` (double bang) is a tricky way to check anything truthy or falsy by avoiding implicit type conversion of `==` comparison.

    - `-5 % 2`

      - **Answer**: `-1`. the result of remainder always get the symbol of first operand

    - `2 in [1, 2]`

      - **Anwser**: `false`. Because "in" returns whether a particular property/index available in the `Object`. In this case object has index 0 and 1 but don't have 2. Hence you get `false`.

    - For `var a = (2, 3, 5);` what is the value of `a`?

      - **Answer**: `5`. The comma operator evaluates each of its operands (from left to right) and returns the value of the last operand.

13. How could you set a prefix before everything you log? for example, if you `log('my message')` it will log: `"(app) my message"`

    - **Answer**: Just get the arguments, convert it to an array and unshift whatever prefix you want to set. Finally, use apply to pass all the arguments to console.

      ```js
      function log(){
        var args = Array.prototype.slice.call(arguments);
        args.unshift('(app)');
        console.log.apply(console, args);
      }

      log('my message'); //(app) my message
      log('my message', 'your message'); //(app) my message your message
      ```

14. Does JavaScript pass parameter by value or by reference? Give an example.

    - **Answer**: Primitive type (string, number, etc.) are passed by value and objects are passed by reference. If you change a property of the passed object, the change will be affected. However, you assign a new object to the passed object, the changes will not be reflected.

      ```js
      var num = 10,
        name = "Addy Osmani",
        obj1 = {
          value: "first value"
        },
        obj2 = {
        value: "second value"
        },
        obj3 = obj2;

      function change(num, name, obj1, obj2) {
          num = num * 10;
          name = "Paul Irish";
          obj1 = obj2;
          obj2.value = "new value";
      }

      change(num, name, obj1, obj2);

      console.log(num); // 10
      console.log(name);// "Addy Osmani"
      console.log(obj1.value);//"first value"
      console.log(obj2.valuee);//"new value"
      console.log(obj3.valuee);//"new value"  
      ```

15. How could you implement cache to save calculation time for a recursive fibonacci function?

    - **Answer**: You could use poor man's memoization with a global variable. If fibonacci is already calculated it is served from the global memo array otherwise it is calculated.

      ```js
      var memo = [];

      function _fibonacci(n) {
        if(memo[n]){
          return memo[n];
        }
        else if (n < 2){
          return 1;
        }else{
          fibonacci(n-2) + fibonacci(n-1);
        }
      }
      ```
