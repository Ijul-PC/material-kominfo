# FE FGD Review Points

## I. Below are assessment aspects that can be used to conduct assessments for FE FGD participants

### A. PERSONALITY

|Aspect|Desc|
|-|-|
|Self Confidence|Self-confidence to show the potential and ability they have|
|Emotional Maturity|The ability to control emotions when in a situation full of conflict|
|Social Maturity|The ability to warmly and openly build social relationships with other people|
|Self-Adjustment|The ability to adapt quickly when in a new situation|
|Independence|The ability to act without the need for encouragement and direction from others|
|Self Integrity|Acting honestly, respecting confidentiality, holding ethical principles and standards|

### B. MANAGERIAL ASPECTS

|Aspect|Desc|
|-|-|
|Planning|The ability to set goals that will be achieved systematically|
|Leadership|The ability to lead and organize others to be willing to follow the goals they want to achieve|
|Decision Making|The ability to make the right decision in a short time|
|Problem Solving|The ability to solve problems appropriately based on facts and rational analysis|
|Developing Others|The ability to encourage others to develop their potential|
|Supervision and Control|The ability to direct, monitor and evaluate the work of subordinates|
|Team Work||

### C. OTHERS

|Aspect|Desc|
|-|-|
|Personal Appearance|Suitability of appearance with the situation of his work environment|
|Attitude of Discipline|Complete a task according to the procedure and complete it thoroughly and on time|
|Motivation for Achievement|Encouragement to always improve themselves towards achieving better achievements|
|Ability to cooperate|The ability to establish working relationships effectively in a group|
|Communication Skills|The ability to hear and express thoughts and feelings persuasively|

## II. Below are review points from selected aspects above for FE FGD, but can be changed according to conditions

<table>
  <tr style = "text-align:center; font-weight:bold;">
    <td rowspan = 2>Candidate</td>
    <td colspan = 6>Aspects</td>
  </tr>
  <tr style = "text-align:center; font-weight:bold; font-style:italic;">
    <td>Leadership</td>
    <td>Communication</td>
    <td>Planning</td>
    <td>Team Work</td>
    <td>Critical Thinking</td>
    <td>Technical Skills</td>
  </tr>
  <tr>
    <td>Judika</td>
    <td>1-5</td>
    <td>1-5</td>
    <td>1-5</td>
    <td>1-5</td>
    <td>1-5</td>
    <td>1-5</td>
  </tr>
</table>
