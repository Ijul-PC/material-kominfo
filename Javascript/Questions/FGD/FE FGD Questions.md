# Frontend FGD Questions

1. What excites or interests you about coding?
2. What is a recent technical challenge you experienced and how did you solve it?
3. When building a new web site or maintaining one, can you explain some techniques you have used to increase performance?
4. Can you describe some SEO best practices or techniques you have used lately?
5. Can you explain any common techniques or recent issues solved in regards to front-end security?
6. What actions have you personally taken on recent projects to increase maintainability of your code?
7. Talk about your preferred development environment.
8. Which version control systems are you familiar with?
9. Can you describe your workflow when you create a web page?
10. If you have 5 different stylesheets, how would you best integrate them into the site?
11. How would you optimize a website's assets/resources?
12. Name 3 ways to decrease page load (perceived or actual load time).
13. If you jumped on a project and they used tabs and you used spaces, what would you do?
14. If you could master one technology this year, what would it be?
15. Explain what ARIA and screenreaders are, and how to make a website accessible.
16. Explain some of the pros and cons for CSS animations versus JavaScript animations.
17. What does CORS stand for and what issue does it address?
18. What resources do you use to learn about the latest in front end development and design?
