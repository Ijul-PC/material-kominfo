# FE FGD SOP

## GET STARTED

### 1. Delivery of Goals (5 min)

- We will talk about the FE Developer role.
- We are interested and want to hear opinions, views and comments, ideas and suggestions from friends about it.
- All answers are TRUE, because we have our own views and opinions about it. All positive and negative opinions can be expressed and accepted.
- You may disagree about what we are talking about. Please all participants to respect the opinions of others. Other people may have different views or opinions and that is a good thing in this discussion.
- Please give everyone the same opportunity to participate in the conversation.

### 2. Procedure (5 min)

- Whatever you will convey in this discussion will be kept and kept confidential and will only be used for the purpose of selecting ___ city FE developer batch candidates ____.
- Because this is a group discussion, your are welcome to express your opinions without having to wait for our appointment. The important thing is that all of you talk one by one, so that we can listen to all of your friends well.

### 3. Introduction (5 min)

- Facilitator's Note: When participants introduce themselves, the facilitator draws a floor plan and number (if needed).
- Please introduce your name and a little information about yourself.

## DISCUSSION

### 1. General View (15-20 min)

- Take 2 or 3 questions from FE FGD General Questions.

### 2. FE Developer (60 min)

- Take 2 or 3 questions from FE FGD Technical Questions.
- Take 2 or 3 questions from JS FGD Questions.

## CLOSING

### 1. Conclusion (10 min)

- Candidates make conclusions from each answer they discuss in the discussion session and explain in front of the facilitators.
