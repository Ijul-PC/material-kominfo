# Exercise #

**You must define the following variables:**

1. A number called myNumber which contains the number 4;
2. A number called myFloat which contains the number 4.5;
3. A string called myString which contains the sentence "Variables are great".;
4. A boolean called myBoolean which contains the value false;

Note: use [JSBin](https://jsbin.com)