# Start Coding
---
Default App.js 
```js
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

```

now delete all of the code in App.js and start code manualy
```js
import React, { Component } from 'react'

import { View, Text } from 'react-native' // Import from library

class App extends Component {

  render() {

    return (

      <View> // <View> imported from react-native library 

        <Text> // <Text> imported from react-native library
          Hello React Native App
        </Text>
      
      </View>
    )
  }
}

export default App; // export this class
```
<img src="./images/first-edit.png" width="300" >

## Styling using StyleSheet
- Import ``StyleSheet`` from react-native
- Create const variable to accomodated style

```js
import React, { Component } from 'react'
import { 
        View, 
        Text, 
        Stylesheet //1. StyleSheet was imported
        } from 'react-native' 
class App extends Component {
  render() {
    return (
      <View> 

        <Text>
          Hello React Native App
        </Text>
      
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
})

export default App;
```
## The Project

<b>In this hanbook we will create movie application with api from tmdb </b>


lets create the lo fi first