# Introduction to HTML

At its heart, HTML is a fairly simple language made up of elements, which can be applied to pieces of text to give them different meaning in a document (Is it a paragraph? Is it a bulleted list? Is it part of a table?), structure a document into logical sections (Does it have a header? Three columns of content? A navigation menu?), and embed content such as images and videos into a page. This module will introduce the first two of these, and introduce fundamental concepts and syntax you need to know to understand HTML.

## Prerequisites

Before starting this module, you don't need any previous HTML knowledge, but you should have at least basic familiarity with using computers and using the web passively (i.e., just looking at it and consuming content). You should have a basic work environment set up, and understand how to create and manage files.

## Guides

This module contains the following articles, which will take you through all the basic theory of HTML and provide ample opportunity for you to test out some skills.

### [Getting started with HTML](01_Getting_Started.md)

Covers the absolute basics of HTML, to get you started — we define elements, attributes, and other important terms, and show where they fit in the language. We also show how a typical HTML page is structured and how an HTML element is structured, and explain other important basic language features.

### [What’s in the head? Metadata in HTML](02_Metadata_in_HTML.md)

The head of an HTML document is the part that is not displayed in the web browser when the page is loaded. It contains information such as the page `<title>`, links to CSS (if you want to style your HTML content with CSS), links to custom favicons, and metadata (data about the HTML, such as who wrote it, and important keywords that describe the document).

### [HTML text fundamentals](03_HTML_Text_Fundamentals.md)

One of HTML's main jobs is to give text meaning (also known as semantics), so that the browser knows how to display it correctly. This article looks at how to use HTML to break up a block of text into a structure of headings and paragraphs, add emphasis/importance to words, create lists, and more.

### [Creating hyperlinks](04_Hyperlinks.md)

Hyperlinks are really important — they are what makes the web a web. This article shows the syntax required to make a link, and discusses best practices for links.

### [Advanced text formatting](05_Advanced_Text_Formatting.md)

There are many other elements in HTML for formatting text that we didn't get to in the HTML text fundamentals article. The elements in here are less well-known, but still useful to know about. In this article you'll learn about marking up quotations, description lists, computer code and other related text, subscript and superscript, contact information, and more.

### [Document and website structure](06_Document_and_Website_Structure.md)

As well as defining individual parts of your page (such as "a paragraph" or "an image"), HTML is also used to define areas of your website (such as "the header," "the navigation menu," or "the main content column.") This article looks into how to plan a basic website structure and how to write the HTML to represent this structure.

### [Debugging HTML](07_Debugging_HTML.md)

Writing HTML is fine, but what if something goes wrong, and you can't work out where the error in the code is? This article will introduce you to some tools that can help.

## Table of Contents

- [Getting started with HTML](01_Getting_Started.md#getting-started-with-html)
  - [Table of Contents](01_Getting_Started.md#table-of-contents)
  - [What is HTML?](01_Getting_Started.md#what-is-html)
  - [Anatomy of an HTML](01_Getting_Started.md#anatomy-of-an-html)
    - [Nesting elements](01_Getting_Started.md#nesting-elements)
    - [Block versus inline elements](01_Getting_Started.md#block-versus-inline-elements)
    - [Empty elements](01_Getting_Started.md#empty-elements)
  - [Attributes](01_Getting_Started.md#attributes)
    - [Boolean attributes](01_Getting_Started.md#boolean-attributes)
    - [Omitting quotes around attribute values](01_Getting_Started.md#omitting-quotes-around-attribute-values)
    - [Single or double quotes?](01_Getting_Started.md#single-or-double-quotes)
  - [Anatomy of a HTML document](01_Getting_Started.md#anatomy-of-a-html-document)
    - [Whitespace in HTML](01_Getting_Started.md#whitespace-in-html)
  - [Entity references: Including special characters in HTML](01_Getting_Started.md#entity-references-including-special-characters-in-html)
  - [HTML comments](01_Getting_Started.md#html-comments)
  - [Summary](01_Getting_Started.md#summary)
  - [References](01_Getting_Started.md#references)
- [What’s in the head? Metadata in HTML](02_Metadata_in_HTML.md#whats-in-the-head-metadata-in-html)
  - [Table of Contents](02_Metadata_in_HTML.md#table-of-contents)
  - [What is the HTML head?](02_Metadata_in_HTML.md#what-is-the-html-head)
  - [Adding a title](02_Metadata_in_HTML.md#adding-a-title)
  - [Metadata: the `<meta>` element](02_Metadata_in_HTML.md#metadata-the-meta-element)
    - [Specifying your document's character encoding](02_Metadata_in_HTML.md#specifying-your-documents-character-encoding)
    - [Adding an author and description](02_Metadata_in_HTML.md#adding-an-author-and-description)
    - [Other types of metadata](02_Metadata_in_HTML.md#other-types-of-metadata)
  - [Adding custom icons to your site](02_Metadata_in_HTML.md#adding-custom-icons-to-your-site)
  - [Applying CSS and JavaScript to HTML](02_Metadata_in_HTML.md#applying-css-and-javascript-to-html)
  - [Setting the primary language of the document](02_Metadata_in_HTML.md#setting-the-primary-language-of-the-document)
  - [Summary](02_Metadata_in_HTML.md#summary)
  - [References](02_Metadata_in_HTML.md#references)
- [HTML text fundamentals](03_HTML_Text_Fundamentals.md#html-text-fundamentals)
  - [Table of Contents](03_HTML_Text_Fundamentals.md#table-of-contents)
  - [The basics: Headings and Paragraphs](03_HTML_Text_Fundamentals.md#the-basics-headings-and-paragraphs)
    - [Implementing structural hierarchy](03_HTML_Text_Fundamentals.md#implementing-structural-hierarchy)
    - [Why do we need structure?](03_HTML_Text_Fundamentals.md#why-do-we-need-structure)
    - [Why do we need semantics?](03_HTML_Text_Fundamentals.md#why-do-we-need-semantics)
  - [Lists](03_HTML_Text_Fundamentals.md#lists)
    - [Unordered](03_HTML_Text_Fundamentals.md#unordered)
    - [Ordered](03_HTML_Text_Fundamentals.md#ordered)
    - [Nesting lists](03_HTML_Text_Fundamentals.md#nesting-lists)
  - [Emphasis and importance](03_HTML_Text_Fundamentals.md#emphasis-and-importance)
    - [Emphasis](03_HTML_Text_Fundamentals.md#emphasis)
    - [Strong importance](03_HTML_Text_Fundamentals.md#strong-importance)
    - [Italic, bold, underline...](03_HTML_Text_Fundamentals.md#italic-bold-underline)
  - [Summary](03_HTML_Text_Fundamentals.md#summary)
  - [References](03_HTML_Text_Fundamentals.md#references)
- [Creating hyperlinks](04_Hyperlinks.md#creating-hyperlinks)
  - [Table of Contents](04_Hyperlinks.md#table-of-contents)
  - [What is a hyperlink?](04_Hyperlinks.md#what-is-a-hyperlink)
  - [Anatomy of a link](04_Hyperlinks.md#anatomy-of-a-link)
    - [Adding supporting information with the title attribute](04_Hyperlinks.md#adding-supporting-information-with-the-title-attribute)
    - [Block level links](04_Hyperlinks.md#block-level-links)
  - [A quick primer on URLs and paths](04_Hyperlinks.md#a-quick-primer-on-urls-and-paths)
    - [Document fragments](04_Hyperlinks.md#document-fragments)
    - [Absolute versus relative URLs](04_Hyperlinks.md#absolute-versus-relative-urls)
  - [Link best practices](04_Hyperlinks.md#link-best-practices)
    - [Use clear link wording](04_Hyperlinks.md#use-clear-link-wording)
    - [Use relative links wherever possible](04_Hyperlinks.md#use-relative-links-wherever-possible)
    - [Linking to non-HTML resources — leave clear signposts](04_Hyperlinks.md#linking-to-non-html-resources--leave-clear-signposts)
    - [Use the download attribute when linking to a download](04_Hyperlinks.md#use-the-download-attribute-when-linking-to-a-download)
  - [E-mail links](04_Hyperlinks.md#e-mail-links)
    - [Specifying details](04_Hyperlinks.md#specifying-details)
  - [Summary](04_Hyperlinks.md#summary)
  - [References](04_Hyperlinks.md#references)
- [Advanced text formatting](05_Advanced_Text_Formatting.md#advanced-text-formatting)
  - [Table of Contents](05_Advanced_Text_Formatting.md#table-of-contents)
  - [Description lists](05_Advanced_Text_Formatting.md#description-lists)
  - [Quotations](05_Advanced_Text_Formatting.md#quotations)
    - [Blockquotes](05_Advanced_Text_Formatting.md#blockquotes)
    - [Inline quotations](05_Advanced_Text_Formatting.md#inline-quotations)
    - [Citations](05_Advanced_Text_Formatting.md#citations)
  - [Abbreviations](05_Advanced_Text_Formatting.md#abbreviations)
  - [Marking up contact details](05_Advanced_Text_Formatting.md#marking-up-contact-details)
  - [Superscript and subscript](05_Advanced_Text_Formatting.md#superscript-and-subscript)
  - [Representing computer code](05_Advanced_Text_Formatting.md#representing-computer-code)
  - [Marking up times and dates](05_Advanced_Text_Formatting.md#marking-up-times-and-dates)
  - [Summary](05_Advanced_Text_Formatting.md#summary)
  - [References](05_Advanced_Text_Formatting.md#references)
- [Document and website structure](#document-and-website-structure)
  - [Table of Contents](06_Document_and_Website_Structure.md#table-of-contents)
  - [Basic sections of a document](06_Document_and_Website_Structure.md#basic-sections-of-a-document)
  - [HTML for structuring content](06_Document_and_Website_Structure.md#html-for-structuring-content)
  - [HTML layout elements in more detail](06_Document_and_Website_Structure.md#html-layout-elements-in-more-detail)
    - [Non-semantic wrappers](06_Document_and_Website_Structure.md#non-semantic-wrappers)
    - [Line breaks and horizontal rule](06_Document_and_Website_Structure.md#line-breaks-and-horizontal-rule)
  - [Planning a simple website](06_Document_and_Website_Structure.md#planning-a-simple-website)
  - [Summary](06_Document_and_Website_Structure.md#summary)
  - [References](06_Document_and_Website_Structure.md#references)
- [Debugging HTML](07_Debugging_HTML.md#debugging-html)
  - [Table of Contents](07_Debugging_HTML.md#table-of-contents)
  - [Debugging isn't scary](07_Debugging_HTML.md#debugging-isnt-scary)
  - [HTML and debugging](07_Debugging_HTML.md#html-and-debugging)
    - [Permissive code](07_Debugging_HTML.md#permissive-code)
    - [HTML validation](07_Debugging_HTML.md#html-validation)
  - [Summary](07_Debugging_HTML.md#summary)
  - [References](07_Debugging_HTML.md#references)

## References

1. https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML
